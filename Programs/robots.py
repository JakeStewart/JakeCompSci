from gasp import *
import random


begin_graphics()
done = False
junk = []

class Player:
    pass

class Robot:
    pass

def collided(thing1, thing2):
    for robot in thing2:
        if thing1.x == robot.x and thing1.y == robot.y:
            return True
    return False

def robot_crashed(bot):
    for a_bot in robots:
        if a_bot == bot:
            return False
        if a_bot.t == 1 and a_bot.x == bot.x and a_bot.y == bot.y:
            robot = Robot()
            robot.t = 0
            robot.x = a_bot.lx
            robot.y = a_bot.ly
            robot.r = Box((a_bot.lx * 10 + 5, a_bot.ly * 10 +5), 10, 10, filled = False)
            living_robots.append(robot)
            return a_bot
        if a_bot.x == bot.x and a_bot.y == bot.y:
            return a_bot
    return False

def place_robots(numbots):
    global robots
    robots = []
    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0,63)
        robot.y = random.randint(0,47)
        if len(robots) % 5 != 0:    
            if not collided(robot, robots):
                robot.t = 0
                robot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, filled = False)
                robots.append(robot)
        else:
            if not collided(robot, robots):
                if level != 4:
                    rtype = random.randint(1, 2)
                else:
                    rtype = random.randint(1,3)
                if rtype == 1:
                    robot.t = 1
                    robot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, color = color.RED, filled = True)
                    robots.append(robot)
                elif rtype == 2:
                    robot.t = 2
                    robot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, color = color.BLUE, filled = True)
                    robots.append(robot)
                elif rtype == 3:
                    robot.t = 3
                    robot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, color = color.YELLOW, filled = True)
                    robots.append(robot)

def place_player():
    global c, player, robots
    player = Player()
    player.x = random.randint(0,63)
    player.y = random.randint(0,47)
    while collided(player, robots) == True:
        player.x = random.randint(0,63)
        player.y = random.randint(0,47)
    c = Circle((player.x * 10 + 5, player.y * 10 +5), 5, filled = True)

def move_robots():
    global robots
    global player
    last_location = []
    for robot in robots:
        if robot.t == 1 or robot.t == 3:
            robot.lx = robot.x
            robot.ly = robot.y
        if robot.t != 2:
            am = 1
        else: 
            am = 2
        if player.x > robot.x:
            robot.x += am
        elif player.x < robot.x:
            robot.x -= am
        if player.y > robot.y:
            robot.y += am
        elif player.y < robot.y:
            robot.y -= am
        move_to(robot.r, (robot.x * 10 + 5, robot.y * 10 + 5))

def move_player():
    global c, player
    key = update_when('key_pressed')
    while key == "r":
        remove_from_screen(c)
        place_player()
        key = update_when('key_pressed')
    if key == "w" and player.y < 47:
        player.y += 1
    elif key == "a" and player.x > 0:
        player.x -= 1
    elif key == "s" and player.y > 0:
        player.y -= 1
    elif key == "d" and player.x < 63:
        player.x += 1
    elif key == "q" and player.x > 0 and player.y < 47:
        player.x -= 1
        player.y += 1
    elif key == "e" and player.x < 63 and player.y < 47:
        player.x += 1
        player.y += 1
    elif key == "z" and player.x > 0 and player.y > 0:
        player.x -= 1
        player.y -= 1
    elif key == "c" and player.x < 63 and player.y > 0:
        player.x += 1
        player.y -= 1
    move_to(c, (player.x * 10 + 5, player.y * 10 + 5)) 

def check_collisions():
    global done, robots, junk, level, living_robots
    living_robots = []
    for bot in robots:
        if collided(bot, junk):
            if bot.t != 3:
                continue
            else:
                robot = Robot()
                robot.t = 0
                robot.x = bot.lx
                robot.y = bot.ly
                robot.r = Box((bot.lx * 10 + 5, bot.ly * 10 +5), 10, 10, filled = False)
                living_robots.append(robot)
                for a in junk:
                    if a.x == bot.x and a.y == bot.y:
                        junk.remove(a)
                        remove_from_screen(a.r)
                        a.t
                        a.r = Box((bot.lx * 10 + 5, bot.ly * 10 +5), 10, 10, filled = False)
                        living_robots.append(a)
                continue
        if collided(player, robots+junk):
                done = True
                Text("You loose!", (320, 240), size = 50)
                sleep(3)
                return
        jbot = robot_crashed(bot)
        if not jbot:
            living_robots.append(bot)
        else:
            remove_from_screen(jbot.r)
            jbot.r = Box((bot.x * 10 + 5, bot.y * 10 +5), 10, 10, filled = True)
            junk.append(jbot)
    robots = []
    for a in living_robots:
        if not collided(a, junk):
            robots.append(a)
    if not robots:
        if level != 4:
            Text(f"Level {level} complete!", (320, 240), size = 50)
            sleep(3)
            clear_screen()
            place_player()
            level += 1
            place_robots(level*5)
        else:
            Text("You win!", (320, 240), size = 50)
            sleep(3)
            return
 

level = 1
place_robots(5)
place_player()

while not done:
    move_player()
    move_robots()
    check_collisions()

end_graphics()
