from adafruit_circuitplayground import cp
import random

player_position = 2
enemy_position = 7
enemy_direction = random.randint(1, 2)
player_has_moved = False
while True:
    cp.pixels.brightness = .3
    if player_position == enemy_position:
        for a in range(10):
            cp.pixels[a] = (255, 0, 0)
        break
    if cp.button_a:
        if player_position != 0:
            player_position -= 1
        else:
            player_position = 9
        player_has_moved = True
    else if cp.button_b:
        if player_position != 9:
            player_position += 1
        else:
            player_position = 0
        player_has_moved = True
    
    if player_has_moved:
        if random.randint(1, 10) == 1:
            if enemy_direction == 2:
                enemy_direction = 1
            else:
                enemy_direction = 2
        if enemy_direction == 2:
            if enemy_position != 0:
                enemy_position -= 1
            else:
                enemy_position = 9
        else:
            if enemy_position != 9:
                enemy_position += 1
            else:
                enemy_position = 0
        player_has_moved = False
    
    for a in range(10):
        cp.pixels[a] = (220, 220, 220)
    cp.pixels[player_position] = (0, 0, 255)
    cp.pixels[enemy_position] = (255, 0, 0)
