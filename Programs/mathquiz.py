# Conrad bug tested and helped me with python syntax, so he gets some credit
from random import randint      #Importing libraries
import random
import operator
import time
import math
ops = {                         #A map used for later purposes
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "//": operator.floordiv,
        "/": operator.floordiv,
        "^": operator.pow
    }
questionamount = int(input("How many questions would you like? - "))    #Asking for question amount
if questionamount < 0:
    abs(questionamount)                                                 #Absolute valuing the input in case of an accidental negative input
randmin = float(input("What would you like the smallest possible number to be? - "))            #Asking for min and max numbers
randmax = float(input("What would you like the largest possible number to be? - "))
if randmin == 0 or randmax == 0:
    print("Either the min or max is 0, they are now being set to 1 and 10")             #Checking for  and fixing if the inputs were 0
    randmin = 1
    randmax = 10
if randmin < 0 or randmax < 0:
    print("You entered a negative for one of your inputs. It will now be made positive")
    abs(randmin)                                              #Checking for and fixing if the inputs were negative, as this can cause one of the random numbers to be 0, which python for some reason hates
    abs(randmax)
if randmin > randmax:
    print("Sorry but you made the min greater than the max. I'm going to swap them now")
    randmax2 = randmax                                                  #Checking for if the min is greater than the max, and switching if so
    randmax = randmin
    randmin = randmax2
if randmax == randmin:
    print("You made the max and min the same value, they will now be set to 1 and 10")  
    randmax = 10                                                                        #Checking for if the inputs were the same value. While this isn't nessecary, it would make for a pretty boring test
    randmin = 1
if randmax > 1000:
    print("I'm sorry, but you made the number too large, and it might crash the program. I'm going to reduce it to 1000 now")
    randmax = 1000
opam = int(input("How many operators would you like to use? min:1 max:5 - "))   #Asking for number of operators, which then regulates to the if statements at the bottom.
if opam < 1:
    opam = 1            #Checking if the input went outside of the 1-5 range and rounding it if so
if opam > 5:
    opam = 5

def startquiz():                        #I'd reccomend looking at the if statements below first, then come back and look at this
    startglobal = time.time()   #Gets the current time
    amountright = 0     #Creating variables
    inisflo = 1
    if randmin%1 == 0 and randmax%1 == 0:       #Checking if the input was an integer
        inisflo = 0
    for a in range (questionamount):    #Loop for the amount of questions
        startlocal = time.time()        #Gets current time again
        if inisflo == 0:                        #If the input was a integer, we make all the outputs integers
            num1 = randint(randmin,randmax)
            num2 = randint(randmin,randmax)
        elif inisflo == 1:                      #If the input was a decimal, we make all the outputs decimals rounded to the 10ths place
            num1 = round(random.uniform(randmin, randmax),1)
            num2 = round(random.uniform(randmin, randmax),1)
        if opam == 1:
            opuse = opin1
        if opam == 2:
            opuse = random.choice([opin1,opin2])        #Picks which input to use as the operator for this question
        if opam == 3:
            opuse = random.choice([opin1,opin2,opin3])
        if opam == 4:
            opuse = random.choice([opin1,opin2,opin3,opin4])
        if opam == 5:
            opuse = random.choice([opin1,opin2,opin3,opin4,opin5])
        op = ops[opuse]                                 #Using the map above, maps the inputed symbol to a form that python lets me store in a variable
        compan = op(num1,num2)                          #Gets the answer
        compans = round(compan,1)                       #With decimals, it sometimes likes to add unnessecary decimal places, this just forces it to the 10ths
        ans = float(input(f"What is {num1} {opuse} {num2}? - "))        #Ask the question using the numbers and the non-translated form of the operator
        if ans == compans:                              #Check if the answer is correct
            print("Congrats! You are right!")           #Congratulate and add to the score
            amountright += 1
        else:
            print(f"Sorry, that is incorrect. The correct answer is {compans}")         #Tells you you were wrong, and tells you the correct answer
        endlocal = time.time()          #Gets current time
        qtime = round((endlocal-startlocal),1)          #Subtracts time from the start of the quesiton to the answered question, getting the elapsed time, then rounds to the 10ths place
        print(f"It took you {qtime} seconds to answer this question")   #Tells how long it took you
    endglobal = time.time()                     #We're now outside of the loop, so everything here only happens once the quiz is over. We get the local time
    ttime = round((endglobal-startglobal),1)    #Same thing as above, subtracting then rounding
    print(f"You got {amountright} out of {questionamount} correct")     #Tells you your score
    print(f"It took you {ttime} seconds to comeplete this quiz")        #Tells you total time for the quiz
    if amountright < (questionamount/3):                                #If your score is less than 33% it gives you advice on how to make it easier                                
        print("That is not a good score. If you are struggling, make your min and max numbers lower, or, if you are using decimals, consider switching back to integers")
    elif questionamount > amountright > ((questionamount/3)*2):         #If your score is more than 66%, tells you you did good, but can do better
        print("Thats a pretty good score, but you could do better")
    elif amountright == questionamount:                                 #If you got a perfect score, you get congratulated
        print("Congrats! You got a perfect score!")
    else:                                                               #If your score was between 33-66%, you get told you did ok, but can definitley do better
        print("You did ok, but you could definitley do better")
                                                            #From here onward we are out of the function
askforop = "What operator would you like to use? Valid inputs are: +, -, *, ^, and //. Keep in mind // is floor division, and ^ is exponents. Please only put one function in at a time. Putting a operator in twice will increase the odds of that operator showing up in your quiz. - "
if opam == 1:                                                           #If you selected one operator
    opin1 = input(askforop)      #Ask what operator
    startquiz()                 #Call the function that starts the quiz
if opam == 2:                           #If you chose 2 operators
    opin1 = input(askforop)     #Ask for what operator
    opin2 = input(askforop)
    startquiz()                 #Call the function to start the quiz
if opam == 3:                           #If you chose 3 operators
    opin1 = input(askforop)
    opin2 = input(askforop)     #Ask for what operator
    opin3 = input(askforop)
    startquiz()                 #Call the function to start the quiz
if opam == 4:
    opin1 = input(askforop)     #Ask for what operator
    opin2 = input(askforop)
    opin3 = input(askforop)
    opin4 = input(askforop)
    startquiz()                 #Call the function to start the quiz
if opam == 5:
    opin1 = input(askforop)     #Ask for what operator
    opin2 = input(askforop)
    opin3 = input(askforop)
    opin4 = input(askforop)
    opin5 = input(askforop)
    startquiz()                 #Call the function to start the quiz
