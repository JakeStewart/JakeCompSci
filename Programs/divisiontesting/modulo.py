print("Modulo, or remainder, returns the remainder when you divide two numbers.")
print("For example, say you have 102 % 20")
print(f"That would give you {102 % 20}")

