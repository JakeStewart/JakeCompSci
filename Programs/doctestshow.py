import doctest
def even_product(end_num):
    """
    >>> even_product(4)
    8
    >>> even_product(2)
    2
    """
    total = 1
    for a in range(2, end_num+1, 2):
        total *= a
    return total
print(even_product(4))

def things_to_add(ran):
    """
    >>> things_to_add(range(1, 6))
    15
    >>> things_to_add(range(1, 3))
    3
    """
    total = 0
    for a in ran:
        total += a
    return total
print(things_to_add(range(1,6)))

def product5(endnum):
    """
    >>> product5(25)
    375000
    >>> product5(10)
    50
    """
    product = 1
    for i in range(5, endnum + 1, 5):
        product *= i
    return product
print(product5(25))

def average_odd(end_num):
    """
    >>> average_odd(5)
    3.3
    >>> average_odd(7)
    4.2
    """
    total = 1
    count = 0
    for a in range(1, end_num + 1, 2):
        total += a
        count += 1
    total /= count
    total = round(total, 1)
    return total
print(average_odd(7))

if __name__ == "__main__":
    doctest.testmod()


