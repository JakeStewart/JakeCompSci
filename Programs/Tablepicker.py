import random, time

names = ["Lary", "Kiersten", "Eimi", "Blu", "Mayah", "Anupama", "Emory", "Jake", "Toby", "Conrad", "Grant", "Sean", "Christian", "Ronan", "Gabriel", "Evan", "Xander", "Jack", "Alex", "Noah", "Kellan", "Rockwell"]

random.shuffle(names)

for a in range(6):
    time.sleep(.5)
    print(f"Table {chr(a+65)} will have:")
    if a > 1:
        for a in range(4):
            time.sleep(.5)
            print(names[0])
            del names[0]
    else:
        for a in range(3):
            time.sleep(.5)
            print(names[0])
            del names[0]


