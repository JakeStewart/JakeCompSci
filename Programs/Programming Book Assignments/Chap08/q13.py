# Step 1: Initialize accumulator
product = 1  # Initialize to multiplication identity
# Step 2: Get data
number = 1
# Step 3: Loop through the data
while number < 21:
    # Step 4: Accumulate
   product = product * number
   number += 2
# Step 5: Process result
print(product)
