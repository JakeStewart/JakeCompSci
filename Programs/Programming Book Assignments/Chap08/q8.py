def sum_func(start, stop):
    counter = start
    sum = 0
    while counter <= stop:
        sum += counter
        counter += 1
    return sum

print(sum_func(1, 10))
