product = 1  # Start with multiplication identity
number = 1
while number < 11:
    product = product * number
    number += 1
print(product)
