funds = 25
miles_per_gallon = 40
price_per_gallon = 3.65
num_gallons = funds / price_per_gallon
num_miles = miles_per_gallon * num_gallons
print(f"The number of miles you can drive on 25 dollars is {num_miles}")
