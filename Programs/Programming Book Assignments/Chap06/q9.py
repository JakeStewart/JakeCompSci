def square(turtle, size):
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)
    turtle.forward(size)
    turtle.right(90)

from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
square(malik, 50)           # draw a square with malik

