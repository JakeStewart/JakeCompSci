from turtle import *
space = Screen()
t = Turtle()
t2 = Turtle()

def turtle_drawing(turtle1, turtle2, distance, angle):
        turtle1.left(angle)
        turtle2.right(angle)
        turtle1.forward(distance)
        turtle2.forward(distance)

turtle_drawing(t, t2, 100, 45)

