def move(turtle, size, angle):
    turtle.forward(size)
    turtle.right(angle)
    turtle.forward(size)
    turtle.right(angle)
    turtle.forward(size)
    turtle.right(angle)
    turtle.forward(size)
    turtle.right(angle)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)

