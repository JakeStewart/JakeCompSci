from turtle import *
space = Screen()
t = Turtle()
def sq(tur, c, dist):
    tur.color(c)
    for a in range(4):
        tur.forward(dist)
        tur.right(90)
def qusq(tur, dis, ang, c1, c2, c3, c4):
    tur.right(ang)
    sq(tur, c1, dis)
    tur.right(90)
    sq(tur, c2, dis)
    tur.right(90)
    sq(tur, c3, dis)
    tur.right(90)
    sq(tur, c4, dis)
    tur.right(90)
qusq(t, 100, 45, "blue", "red", "green", "yellow")
