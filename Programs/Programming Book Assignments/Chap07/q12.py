def factorial(n):
    fact = 1
    top = n + 1
    for i in range(1, top, 1):    # Count down from n to 1
        fact = fact * i             # Accumulate the product
    return fact

print(factorial(2))    
