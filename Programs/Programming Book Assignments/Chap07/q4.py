product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for total in numbers:
    product = product * total
print(product)          # Print the result
