def pro(num):
    product = 1  # Start out with 1
    for number in num:
        product = product * number
    return(product)
numbers = [1, 2, 3, 4, 5]
print(pro(numbers))
