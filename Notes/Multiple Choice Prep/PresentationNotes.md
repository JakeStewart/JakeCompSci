# Presentation Notes

## Presentation 1: Noah, Anupama, Eimi, and Rockwell

### Notes

- Know all 5 big ideas
- Focus on the ones worth more
- If you don't know a question, skip over it and come back
- Practice tests
- Use resources
- "Repitition Helps" I personally disagree with this, but everyone learns differently

Sidenote: If you repeat a resource, deep dive into it

### Personal Assessment

- Khan Adademy
- The College Board Website
- Test Guide

## Practice Questions

[source](https://www.test-guide.com/quiz/ap-comp-sci-2.html)

### Question 1:

The employees of an organization receive an email from the organization's payroll department. It states that the employees must send their updated banking information by the end of the day so their paychecks can be transferred to their accounts.

Upon asking the payroll department, they deny ever sending such an email. What is this an example of?

- [ ] Keylogger Attack
- [ ] Computer Virus
- [ ] Maleware Insertion Point
- [x] Phishing Attack

### Question 2:

An organization requires multi-factor authentication before giving the site access to its employees. One of the authentication factors requires an employee to use a security token that is texted to their mobile phones.

What factor is being used to identify the employee?

- [ ] Inheritance
- [ ] Knowledge
- [ ] Facial Features
- [x] Possession

### Question 3:

When customers log into their account on an online shirts retail store, they can access information about the selected shirts before making their purchase.

Some information about the shirts includes ID, Type, price, size, color, and available units of the selected shirt. What additional information can be derived from the available dataset?

- [ ] # of shirts sold in prev. moth
- [x] What shirt is currently out of stock
- [ ] What type of shirt is popular among children
- [ ] What color shirt men perfer
