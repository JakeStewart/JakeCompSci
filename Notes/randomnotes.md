# Doctest
if __name__ == "__main__":
    import doctest
    doctest.testmod()

Put it at the bottom of any program that has doctest.
If it fails, it gives you an error, if it suceeds, it gives nothing.
To give it a test, put:
"""
>>>yourcodehere
resultyouwant
"""
___
