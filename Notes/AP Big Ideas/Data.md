# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- Abstraction - Removing parts that aren't important
- Analog data - Data represented phisically
- Bias - When a program is made without thought for a particular group of people
- Binary number system -A base two number system
- Bit - The smallest the binary system goes, one digit of 1 or 0
- Byte - 8 bits
- Classifying data - Identifying and sorting unstructured data
- Cleaning data - Fixing/removing corrupted, incorrect, or otherwise bad data
- Digital data - Data represented digitally
- Filtering data - Narrowing down the data set
- Information - Data that is organized
- Lossless data compression - A way of compressing data that does not result in a loss of data
- Lossy data compression - A way of compressing data that does result in a loss of data
- Metadata - Data that provides info on other data
- Overflow error - When you try and represent too much data with too little of a storage size
- Patterns in data - Features and tendancies in data
- Round-off or rounding error - A computer not being able to represent a number exactly
- Scalability - A computer being able to use more or less resources depending on what it needs
