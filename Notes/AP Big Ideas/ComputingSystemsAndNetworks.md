# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth - How much data can transmitted in *x* amount of time
- Computing device - Something that can do math automatically
- Computing network - A bunch of devices working together
- Computing system - A bunch of devices with a central storage
- Data stream - Data being sent
- Distributed computing system - Essentially combining a bunch of computers into one supercomputer
- Fault-tolerant - A device still working when things break
- Hypertext Transfer Protocol (HTTP) - Set of rules for data transfer
- Hypertext Transfer Protocol Secure (HTTPS) - HTTP but secure
- Internet Protocol (IP) address - Like an adress but for computers to be found on the internet
- Packets - A chunk of data transmitted
- Parallel computing system - Dividing up a task amoung multiple processors
- Protocols - Rules on data transfer
- Redundancy - Making backups for something in case it breaks
- Router - Connects two networks
