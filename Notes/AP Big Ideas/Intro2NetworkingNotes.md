# Intro to Networking Notes

## Chapter 1

### Old methods
- People used to just wire two places together that needed to talk, which sometimes lead to one place having dozens or hundreds of wires
- This didn't work great for long-distance
- So, telephone companies made a bunch of hubs that would connect many different lines
- But, each line could only handle one call at a time

### Newer methods

- Computers needed a constant connection with each other, so at first, lines would be leased from telephone companies
- Eventually, someone figured out how to essentailly dasiy chain computer connections, and the beginnings of the internet started
- Computers would route data to a central computer, which would eventually send it onward
- This could take a very long time, occasionally
- Annother innovation came when people realized that breaking the message into smaller "packets" would make the message much quicker to send, and create less load on the forwarding computer
- As that became more popular, computers specially designed to do that were created, which we now know as routers
- As networks became larger and larger, it became nessecary to create source and destination adresses, which we now know as IPs
- Routers can adapt to breakdowns or lag

## Chapter 2

|TCP/IP Model|
|---|
|Application|
|Transport|
|Internetwork|
|Link|

The model above represents the four parts, or levels of networking. Starting with link, which is your computer to the LAN, and ending with application which is what the user ends up seeing.

### Link

- The link layer is your data moving from the device to a LAN
- This is done wirelessly, like with wifi and cellular, or wired, like with fiber optics or ethernet
- For this to work, people need to agree on certain protocols for sending the data, like radio frequencies
- When you have multiple devices trying to connect to one thing at the same time, they end up taking turns sending packets
- Computers will detect other computers trying to send data, then both will stop
- They then start taking turns

### Internetwork

- Once your packet hits the LAN, its first stop is a router
- Each router will send your packet it's best guess of where your packet will go, then another, etc.
- The closer you get, the more accurate the routers can send you

### Transport

- Sometimes, data gets lost or very delayed
- If some data is missing at the reciving end, the reciving computer will re-reqest the data from the sender
- The sending computer always keeps copies of the data its sending till the reciver says it's recived it
- So, the sending computer has something called a window size, which is how much it sends per go
- To little makes the message slow, as its always waiting for confirmation
- To much overloads things
- The sender will start small, work its way up, then find a balance

### Application

- Application comes in two parts, the 'server' and 'client'
- Server is the one running the thing, and reciving the connection
- Client is you, the person making the connection
- Different applications will use different message protocols, depending on their need


The reason they are stacked like that is because each layer uses all the layers above and below to accomplish it's task

## Chapter 3: Link

Some examples of a link layer are ethernet, WiFi, and the cellular system

### Sharing the Air
- Almost all the time, several computers will be connected to one router, called the "base station" or "gateway"
- All computers connected to the gateway can hear all packets the gateway sends/recives
- This can be a security flaw if someone manages to get onto your network maliciously
- Every radio reciver and sender found in a computer has a unique serial number, called a MAC adress
- A MAC adress is essentially an acctual adress, but for a link layer
- Your computer, when it first connects to a gateway, will ask it for it's MAC adress so it knows where to send it's packets

### Courtesy and Coordination
- When multiple computers are all sending data to a gateway at the same time, everything "collides" and becomes a garbled mess nobody can understand
- Multiple techniques have been made to combat this
- "Carrier Sense" is essentialy just waiting your turn in line to send a message. If another computer is transmitting, wait. This generally just ends up being both computers taking turns sending packets.
- If two computers see nothing is happening, and both decide to send it at the same time, things still could collide
- So, your computer always makes sure it can hear it's own data, otherwise, it assumes it got collided with
- Once this happens, both try again a random amount of time later to minimize the risk of re-colliding
- The official name for all this is CSMA/CD
- The downside is that sometimes, it can take too long or be too costly to detect a collision

### Coordination in Other Link Layers
- Sometimes, a link layer needs multiple transmitters operating at 100% efficency for long amounts of time
- When this happens, a different approach is taken
- The computers will pass around a token, and any computer is only allowed to transmit if they have the token
- Think of it like ordering food at a counter, and waiting for them to call your number to pickup your food
- This ensures that no computer ever collides
- The downside of this is if you only have one computer that needs to transmit, it still needs to wait for the token to be passed around, which makes things siginfigantly slower
- This results in CSMA/CD being used for smaller networks, like Wifi, and token being used for larger networks, like satilites and fiber optics

## Chapter 4: Internetwork

Internetwork is the layer in which your packet goes on a trip to it's destination. It may use multiple different mediums to do this such as WiFi, fiber optics, satillite, etc. The packet will travel along these, bouncing from router to router, each router sending it in the general right direction.

### IP Adresses
- Using the MAC adresses from the link layer would require tracking every device in the world, so we need a different solution
- That solution is IP adresses
- IP adresses are broken up into multiple chunks
- The first part of an IP is the network number. This is used to identify what network a computer is on
- The second part is the host identifier, this tells you what specific computer on the network you are looking for

### Routers Routing
- A factory-fresh router barely knows any routes to send things
- So, when it arrives at a packet that it doesn't know where to send, it asks it's neighbors. If they don't know, they ask theirs, ect. Eventually, the route is found and sent back to the router, who sends it on its way, and stores that info for later use

### When Things Change
- Should a link fail, and a router cannot send things through it anymore, it quickly wipes all routes using that router anymore and re-querries it's neighbors that it can still reach if they know an alternate path.
- After a bit, the change ripples throughout the system, and everything returns to normal
- If there is a lull in communication, a router will ask it's neighbors if they have any new or better routes

### Determining the Whole Route
- No router on the internet knows the whole path a packet will take
- All they can do is get it closer, one step at a time
- Your computer can esitmate where a packet will travel with a traceroute command
- The IP layer has a backup for if several routers form a loop, as that could break several routers
- When a packet is sent, it is designated a Time To Live. Every time it passes through a router, it subtracts 1. When it hits zero, it gets trashed
- The router that trashed it also sends back a message saying it threw it away
- Traceroute works by slowly increasing the TTL of a packet to see the shortest distance needed, creating an approx. path

### Getting an IP Adress
- When a computer moves location, it gets a new IP adress, thanks to DHCP
- It essentially asks the gateway what IP should I use, and the uses it
- Sometimes, it recives no answer so it just says, ima use this IP
- It then proceeds to not work

### Different Adresses
- Non-routeable IPs generally start with 192.168, and are only used locally, with other devices connected to the same gateway, and the gateway itself
- This allows us to conserve acctual adresses

### Global IP Adresses
- When your computer wants to connect to a network it asks the ISP(Internet Service Provider) for an IP
- It gets a range of IP adresses it can have
- At the top level of IP assignment are the 5 RIRs, which control every IP in the world
- When IPs were invented, it was not expected the internet would grow this large, so the old IPv4s all got used up, and now we use the much larger IPv6s
- The RIRs are leading the transition, but it will still take several years to completley phase out IPv4

## Chapter 5: Domains

- Domains are strings that get bound to IPs
- This allows for the user to not have to memorize loads of different IPs for the sites they want to visit
- The computer simply looks up the IP that corresponds to the domain

### Allocating Domain Names

- Domain names are allocated based on who owns them
- ICANN is the organization in charge of domains
- They often give top-level endings (.com, .net, .org) to other organizations to manage
- Country codes like .us, uk, etc. are used to show what country the organization that owns the domain is from
- The organizations that control the top-level domain endings hand out domains to other organizations, who can then assign subdomains
- Top-levels like .org and .com can be bought by individuals

### Reading Domain Names

- Domain names are read from right to left, as the right is the broadest, then it narrows down to the organization itself

## Chapter 6: Transport Layer

- The transport layer is to ensure that all packets make it through the internetworking layer to their destination

### Packet Headers

- Packets have several headers
- These include Link and Internetwork, which contain data for those respective parts
- But it also includes a transport header, known as a TCP
- It holds the data as to where the packet fits into the puzzle that is the message

### Packet Reassembly and Retransmission

- As the reciving computer gets the packets, it looks for where the packet belongs so that it can assemble the packet in the correct order
- To avoid overwhelming the network, the transport layer only sends a certain amount of data before waiting for an agknowledgement from the reciving computer
- That amount of data is called the window size, and it gets adjusted as the sending computer sees that things are coming back quickly or slowly
- This allows for opimal data transmission speeds
- If a packet is lost, both computers end up waiting, for either the confirmation message or the missing packet
- To make sure they don't wait forever, the destination computer will only wait so long in between packets before requesting a retransmission of the data

### Transport Layer in Operation

- One of the key parts of the transport layer is that the sending computer **has** to hold onto the data it is sending until it recives the confirmation message

### Application Clients and servers

- The purpose of the transport layer is to provide reliable connections between multiple applications
- For the application, this is as simple as asking the transport layer to make the connection
- In the connection, the initiating party is called the *client* and the reciving party is called the *server*
- The combination of these two is called the *client/server*

### Server Applications and Ports

- Should a client want to make a conection, it is important that they connect to the right application on the server
- The concept of this is called ports, and they allow the client to specify what application on the server they want to network with
- When a server application starts, it listens for incoming connections on the port it has been given

Well-Known Default Ports include:

|Name|Port|Use|
|---|---|---|
|Telenet|23|Login|
|SSH|22|Secure Login|
|HTTP|80|World Wide Web|
|HTTPS|443|Secure Web|
|SMTP|25|Incoming Mail|
|IMAP|143/220/993|Mail Retrival|
|POP|109/110|Mail Retrival|
|DNS|53|Domain Name Resolution|
|FTP|21|File Transfer|

## Chapter 7: Application Layer

## Client-Server Applications

- There are two parts that a networked application needs to function
- The client, and the server
- To initiate a connection between client and server, the user types a web adress into a web application running on their computer
- This gets looked up into an IP and initiates the connection
- On the other end is a web server, which is always waiting for connections

### Application Layer Protocols

- A client/server application needs protocols to follow
- A protocol is "A rule which describes how an activity should be performed, especially in the field of diplomacy"
- Essentially, they are very precise rules, because computers like precision
- There are many different applications, so it is important that each one is well-documented so that the client and server understand each other
- You can see what kind of protocol is being used at the beginning of some URLS, (ex: "https" is for HTTPS)
- HTTP is one of the most widley used protocols, as it is simple, but has alot of detail that web developers can work with

### Exploring HTTP

- Telenet was one of the first web applications, and is still present on unix-based machines such as linux and mac
- If you were to use telenet to initiate a barebones connection with a computer, then you would have to be very precise
- If you do not follow HTTP, then the server will start getting unhappy very fast, as that is what it's trained to do
- If the client doesn't know protocol, then the server will just straight up shut down your connection
- If you were to send proper protocol, the server will be happy to listen and accomadate your needs

### The IMAP Protocol

- Another common protocol is the IMAP protocol, which handles mail
- Most computers have some form of mail app
- IMAP is a bit more complex than HTTP

### Flow Control

- When an client and server make a connection, they initiate the transport layer to do it's thing
- The ability to start and stop the sending application is called *flow control*
- The applications themselves are not responsible for flow control, the transport layer is the one in charge of that

### Writing Networked Applicaitons

- Networked applications can be written in many different languages these days
- Most languages have libraries that allow for easy networking
- This is because the link, internetwork, and transport layers are so good at their job they let the user worry about the application itself, not all the nitty-gritty networking stuff

## Chapter 8: Secure Transport Layer

- The link, internetwork, and transport layers are focused on moving data around, not keeping that data secure and private
- As the internet grew rapidly, so did the need for a security system
- The solution that was created was to encrypt and decrypt data at the source and destination respectivley

### Encrypting and Decrypting Data

- Encrypting is essentially putting the message into a code that can only be read, or decrypted via some form of secret

### Keys

- Sending a shared secret over the internet defeated the whole point of the encryption process, so another solution was needed
- In the 70's, a solution came in the form of asymmetric keys
- The idea is that one key encrypts the data whilst another decrypts it
- The reciving computer picks both the en and de cryption keys
- The sending computer gets one of the keys, the public key from the reciving computer, which is used to encrypt the message
- The data could only be decrypted with the private key, which never left the computer reciving the data

### SSL

- Because security was added so late after the internet was made, it was important to not break existing things
- The solution was to add an optional layer between the Transport and application layer, the Secure Sockets Layer, or SSL
- If the option was chosen to encrypt the data, the transport layer would encrypt it before sending it
- The SSL would then decrypt it before sending it to the reciving computer

### Encrypting Web Browser Traffic

- You often don't notice when you are using an encrypted vs decrypted applicaiton, the main difference is an "s" after the http (http vs https)
- For a while, security was slightly more costly, so it was only used for bank accounts and such
- As tech has developed though, security got less and less of a hassle, and started getting used for more and more things

### Certificates and Certificate Authorities

- The point of a certificate authority is to check if the public key you are reciving is in fact from the server you want to connect to
- If a key is legitimate, it will be digitally signed by a CA
- If it isn't your computer isn't going to trust it