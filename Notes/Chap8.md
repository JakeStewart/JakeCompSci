# Chapter 8 Notes
---
## Loops in General
- The body of a loop is the inside part, the part being repeated
- A counter is a var used to count something
- Increment means to do += 1
- An infinite loop is one that never ends
- A logical expression is a boolan, a true or false 

## While Loops
- A *while* loop repeats the body for as long as a logical expression is true(ex: a == b)
- A logical expression is a boolean, a true or false
- A *while* loop is useful when you don't know how many times you want to execute the loop(ex: you want the game to keep going *while* there isnt a winner)
- A while loop can be made to go on forever by simply putting in a statement that is always going to be true(ex: 1 == 1)
- You can do the same thing as a *for* loop with a *while* loop by using a counter and having it work *while* the counter is < whatever you want

## For loops
- You can nest loops inside each other(putting a loop inside another loop). This will make the nested loop run its amount times the main loop's amount. This can be useful when you need to generate, say, times tables

