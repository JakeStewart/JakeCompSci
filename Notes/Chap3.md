# Chapter 3: Names for numbers notes

## Variables
- A variable is a name a computer associates with a value
- The value the variable contains can be changed by the computer
- Setting a variable's value is also called an assignment
- A variable name must start with a letter or underscore
- A variable name can contain numbers but not as the first charachter
- A variable name cannot be a python keyword like if, and, etc
- Capitalization does matter, *thing* is different from *Thing*
- No spaces in a variable name, but you can use underscores, or do whats called camel-case and uppercase the first letter of each word where you would have put a space
- You can also set a variable's value to an equasion, like 2 times 2 or 4/2
- The % operator returns the remainder when you divide the first number by the second
### List of Expression types
1. plus sign addition
2. star that you get from shift 8 multiplication
3. / division
4. // floor division
5. % remainder(see above)
6. -1 negative
### Order of operations
1. Negation
2. Multiplication, division, remainder
3. Addition/Subtraction

