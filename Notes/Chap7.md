# Chapter 7 Notes

## For Loops

- Computers can repeat steps infinitley, without getting tired
- A loop is something that repeats
- A *for* loop uses a variable that takes on a list
- A list holds a group of values in a certain order
- It is important to use clear variable names

### Range

- The range function can simply be used to run a loop a certain number of times
- It can also be used with two numbers to assign the variable all the integers inside that range

### Accumulator Pattern

An accumulator pattern has 5 steps
1. Set the variable to an initial value
2. Give it something to work with
3. Go through all the data using a for loop so the var takes every value
4. Combine all pieces
5. Do something with output

All it does is simply go through a list and accumulate it all to one variable using the operator of choice.

### Print()

Print can be used to see what's happening inside of a program. 
By printing it. 
As text. 
It's not complicated.
