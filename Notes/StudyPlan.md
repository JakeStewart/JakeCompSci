# Study Presentation
* Intro
* Resources
	* Notes
	* Online Resources
## Intro


## Resources

There are many resources available to help you study for the exam.

### Notes

One great way to review for the test is to look at your notes on each big idea. They can help you review the subject in a way you understand best, since you made them.

Another good way to review is to look at previous practice tests you may have done. This will help you see how the questions are phrased, and how a test might actually look like. It might also help you review some subjects you aren’t entirely clear on. Alternatively, you could make a copy of the test, and redo it, to see how you might do on an actual exam.

Looking at your studyplan.md tells you how the test is scored, which will give you knowledge on how the test works. It also shows online resources you found, which you can look back on.

### Online Resources

Doing practice questions lets you know if you are ready to take the multiple choice section for the APCSP. An example of a resource where you can do this is [CodeHS APCSP Exam review](https://codehs.com/playlist/ap-cs-principles-exam-review-1780).

Intro/outline(debrief on test) 

Best ways to study for the AP Computer Science Principles Diagnostic Test:

* Past notes on big ideas – Data.md, ComputingSystemsAndNetworks.md and CreativeDevelopment.md	
* Look over study plan - studyplan.md
* Look at notes on classmates repositories -found in Jeff’s website  
* Review past big idea tests - Big Idea 1: Creative Development, Big Idea 2: Data(You should have 2 tests) and Big Idea 4: Computing Systems and Networks
* Review the full test we took in the beginning of the year - Make sure to look at the questions you got wrong on the reference sheet

There are many online resources available to help study for the AP exams. One really good resource is the actual College Board website. They have a list of tips for those taking the APCSP exam. [College Board APCSP Exam Tips] (https://apstudents.collegeboard.org/courses/ap-computer-science-principles/assessment-tips)
Another great resource is Khan Academy. They have a specific course for APCSP and at the end they have an exam preparation course. [Khan Academy Exam Prep](https://www.khanacademy.org/computing/ap-computer-science-principles/ap-csp-exam-preparation)