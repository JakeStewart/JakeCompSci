# Study Plan

## How Tests are scored

It would seem that they get the test scores from comparing AP students to accual colledge students. 
The computer will score the multiple question answers
The open-ended ones are scored by colledge professors
That score is then translated into the 5 point scale to "ensure that, for example, a 3 this year reflects the same level of achievement as a 3 last year"
12.4% got a 5 last year, 21.7% a 4, 32.5% a 3, 19.9% a 2, and 13.6% a 
Found a jackpot- this [link](https://www.albert.io/blog/ap-computer-science-principles-score-calculator/) calculates your score based on the curve of previous year's test scores. Just plug stuff in and you're good.
The create task is worth 30% of your grade
Create task must be done by the 1st of May
You must make a program, videotape it working, and write a written response
Graded based off of [this](https://apcentral.collegeboard.org/media/pdf/ap22-sg-computer-science-principles.pdf) rubric

## How I did on pratice

I did ok on big ideas 4 and 5, with a 66% on each, meh on 1 and 2 and horrible on 3
I feel like this accuratly reflects what I've been taught so far
I think I did so bad on three because I didn't really understand the whole algorithim thing, so that is probably something to look into

### What I can do to Improve

- [] Study pretty much all the subjects, but kinda in this order of importance
1. Algorithms
2. Data
3. Creative Development
4. Computer Systems and Networking
5. Impact of Computers
- [] Figure out what I want to do for the create task
- [] Take some more practice tests

### Resources for studying

[College Board Classroom Resouces](https://apcentral.collegeboard.org/courses/ap-computer-science-principles/classroom-resources)
[Past exam questions](https://apcentral.collegeboard.org/courses/ap-computer-science-principles/exam/past-exam-questions)
[Google](https://www.google.com/)`




