# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The purpose of the program is to play a game.
>
>
2. Describes what functionality of the program is demonstrated in the
   video.
> In the video, you see the player (circle) moving around to make the robots (squares) crash into each other, and make junk (dark squares) that provide a stationary hazard. If the player gets stuck, you can see that they can perform a radom teleport to escape. 
>
>
3. Describes the input and output of the program demonstrated in the
   video.
> Input - Player presses keys to move around
> Output - The robots move twards the player, and the player itself moves
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> ![list1](./Pictures/list1.png)
>
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
> ![list2](./Pictures/list2.png)
>
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> robots
>
>
2. Describes what the data contained in the list represent in your
   program
> The list stores the data of every "robot", such as the location and type
>
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> If the list wasn't used, the robot data would have to be stored in individual variables, decreasing readability and just making the program messy, along with limiting the amount of robots.
>
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![procedure](./Pictures/procedure.png)
>
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> ![procedurecall](./Pictures/call1.png)
>
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> The procedure takes a list and a vaiable, then sees if the variable and list have parts in the same spot. This helps to check for collisions.
>
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> The procedure takes two parameters, a variable and a list. It iterates through the list, in which it checks if the x of the var and x of the current part of the list are the same, and if the ys are the same. The function then returns true if there is a collision. If there is no collisions, then there is a return false after the loop. 
>
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>
> ![call1](./Pictures/call1.png)
>
>
> Second call:
>
>![call2](./Pictures/call2.png)
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>
> A robot, and the list of the dead robots or "junk"
>
>
> Condition(s) tested by the second call:
>
> The player, and the list of the robots
>
>
3. Identifies the result of each call
>
> Result of the first call:
>
> That would depend on if there is a collision or not
>
>
> Result of the second call:
>
> That would also depend on if there is a collision or not
>
>
